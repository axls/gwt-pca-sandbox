package js.d3.force;

import js.d3.common.JsFunction;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public class ForceLink {

    public native <T, R> R id(JsFunction<? super T, ? extends R> mapper);

    public native ForceLink distance(int distance);
}
