package js.d3.force;

import com.axellience.vuegwt.core.client.jsnative.jsfunctions.JsRunnable;

import js.d3.common.JsSupplier;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public interface Simulation {

    Simulation force(String name, Object force);

    Simulation alphaTarget(double decay);

    Simulation restart();

    <T> Simulation on(String typenamesstring, JsSupplier<? extends T> handler);

    Simulation on(String typenamesstring, JsRunnable handler);

    Simulation stop();
}
