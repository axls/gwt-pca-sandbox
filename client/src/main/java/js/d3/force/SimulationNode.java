package js.d3.force;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public class SimulationNode {
    /**
     * Node’s zero-based index into nodes array. This property is set during the initialization process of a simulation.
     */
    public int index;
    /**
     * Node’s current x-position
     */
    public int x;
    /**
     * Node’s current y-position
     */
    public int y;
    /**
     * Node’s current x-velocity
     */
    public int vx;
    /**
     * Node’s current y-velocity
     */
    public int vy;
    /**
     * Node’s fixed x-position (if position was fixed)
     */
    public Double fx;
    /**
     * Node’s fixed y-position (if position was fixed)
     */
    public Double fy;

}
