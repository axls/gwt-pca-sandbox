package js.d3.common;

@jsinterop.annotations.JsFunction
public interface JsBiFunction<T, U, R> {
    R apply(T t, U u);
}
