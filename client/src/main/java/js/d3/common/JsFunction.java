package js.d3.common;

@jsinterop.annotations.JsFunction
public interface JsFunction<T, R> {
    R apply(T arg);
}
