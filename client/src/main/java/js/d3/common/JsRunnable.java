package js.d3.common;

@jsinterop.annotations.JsFunction
public interface JsRunnable {
    void run();
}
