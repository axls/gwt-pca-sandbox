package js.d3.common;

import jsinterop.annotations.JsFunction;

@JsFunction
public interface JsConsumer<T> {
    void accept(T t);
}
