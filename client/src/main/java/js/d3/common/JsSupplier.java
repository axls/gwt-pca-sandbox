package js.d3.common;

@jsinterop.annotations.JsFunction
public interface JsSupplier<T> {
    T get();
}
