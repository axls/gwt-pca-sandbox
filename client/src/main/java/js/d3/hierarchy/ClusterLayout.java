package js.d3.hierarchy;

import js.d3.common.JsFunction;
import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public class ClusterLayout {

    public native int[] size();

    public native ClusterLayout size(int[] size);

    public native int[] nodeSize();

    public native ClusterLayout nodeSize(int[] size);

    @JsOverlay
    public final HierarchyPointNode invoke(HierarchyNode data) {
        return Js.<JsFunction<HierarchyNode, HierarchyPointNode>> cast(this).apply(data);
    };
}
