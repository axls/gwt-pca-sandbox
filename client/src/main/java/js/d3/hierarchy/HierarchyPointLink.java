package js.d3.hierarchy;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public class HierarchyPointLink {

    public HierarchyPointNode source;
    public HierarchyPointNode target;
}
