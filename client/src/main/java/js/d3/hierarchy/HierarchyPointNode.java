package js.d3.hierarchy;

import elemental2.core.JsArray;
import js.d3.common.JsConsumer;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public class HierarchyPointNode extends HierarchyNode {

    public int x;
    public int y;
    public HierarchyPointNode[] children;
    public HierarchyPointNode parent;

    public native void each(JsConsumer<HierarchyPointNode> jsConsumer);

    public native HierarchyPointLink[] links();

    public native JsArray<HierarchyPointNode> descendants();
}
