package js.d3.hierarchy;

import js.d3.common.JsBiFunction;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public class HierarchyNode {
    public int height;
    public Object data;
    public int dx;
    public int dy;

    public native HierarchyNode sort(JsBiFunction<HierarchyNode, HierarchyNode, Integer> comparator);
}
