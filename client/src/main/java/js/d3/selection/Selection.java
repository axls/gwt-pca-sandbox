package js.d3.selection;

import js.d3.common.JsSupplier;
import jsinterop.annotations.JsFunction;
import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public interface Selection {

    @JsFunction
    @FunctionalInterface
    public interface ValueFn {
        Object invoke(double datum, int index);
    }

    @JsFunction
    @FunctionalInterface
    public interface ValueObjectFn<T> {
        Object invoke(T datum, int index);
    }

    @JsFunction
    @FunctionalInterface
    public interface ValueObjectFnNoIndex<T> {
        Object invoke(T datum);
    }

    Selection style(String attr, Object value);

    //Selection attr(String attr, ValueFn value);

    @JsMethod(name = "attr")
    <T> Selection attr(String attr, ValueObjectFn<T> value);

    @JsMethod(name = "attr")
    <T> Selection attr(String attr, ValueObjectFnNoIndex<T> value);

    Selection attr(String attr, Object value);

    Selection selectAll(String selector);

    Selection data(double[] data);

    Selection data(Object[] data);

    Selection enter();

    Selection append(String type);

    Selection text(Object value);

    @JsMethod(name = "text")
    <T> Selection text(ValueObjectFn<T> value);

    @JsMethod(name = "text")
    <T> Selection text(ValueObjectFnNoIndex<T> value);

    Selection join(Object enter);

    <T> Selection filter(ValueObjectFn<T> value);

    Selection clone(boolean deep);

    Selection lower();

    <T, R> Selection call(js.d3.common.JsFunction<? super T, ? extends R> function);

    <T> Selection call(JsSupplier<? extends T> function);

    Selection call(Object function);

    Selection remove();
}
