package js.d3.tree;

import java.util.Collection;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public class TreeNode {

    public String name;
    public TreeNode[] children;

    @JsOverlay
    public static TreeNode create(String name) {
        return create(name, (TreeNode[]) null);
    }

    @JsOverlay
    public static TreeNode create(String name, Collection<? extends TreeNode> children) {
        return create(name, children.toArray(new TreeNode[0]));
    }

    @JsOverlay
    public static TreeNode create(String name, TreeNode[] children) {
        TreeNode result = new TreeNode();
        result.name = name;
        result.children = children;
        return result;
    }

    @JsOverlay
    public final JsPropertyMap<Object> asPropertyMap() {
        return Js.cast(this);
    }
}
