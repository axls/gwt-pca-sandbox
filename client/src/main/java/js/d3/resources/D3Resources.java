package js.d3.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;

public interface D3Resources extends ClientBundle {
    D3Resources INSTANCE = GWT.create(D3Resources.class);

    @Source("d3.js")
    TextResource d3Js();

    @Source("d3.min.js")
    TextResource d3MinJs();

    @Source("d3.css")
    TextResource d3Styles();
}
