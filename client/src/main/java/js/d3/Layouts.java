package js.d3;

import js.d3.hierarchy.ClusterLayout;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public interface Layouts {

    ClusterLayout cluster();
}
