package js.d3.drag;

import js.d3.common.JsConsumer;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public interface DragBehavior<T> {

    DragBehavior<T> on(String typenamesstring, JsConsumer<? super T> handler);
}
