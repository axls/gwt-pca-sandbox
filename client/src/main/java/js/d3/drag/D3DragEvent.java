package js.d3.drag;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;
import jsinterop.base.Any;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public class D3DragEvent {
    /**
     * The DragBehavior associated with the event
     */
    public DragBehavior<?> target;
    /**
     * The event type for the DragEvent
     */
    public String type;
    /**
     * The drag subject, defined by drag.subject.
     */
    public Object subject;
    /**
     * The new x-coordinate of the subject, relative to the container
     */
    public int x;
    /**
     * The new y-coordinate of the subject, relative to the container
     */
    public int y;
    /**
     * The change in x-coordinate since the previous drag event.
     */
    public int dx;
    /**
     * The change in y-coordinate since the previous drag event.
     */
    public int dy;
    /**
     * The string “mouse”, or a numeric touch identifier.
     */
    public Any identifier;
    /**
     * The number of currently active drag gestures (on start and end, not including this one).
     *
     * The event.active field is useful for detecting the first start event and the last end event
     * in a sequence of concurrent drag gestures: it is zero when the first drag gesture starts,
     * and zero when the last drag gesture ends.
     */
    public int active;
    /**
     * The underlying input event, such as mousemove or touchmove.
     */
    public Any sourceEvent;
}
