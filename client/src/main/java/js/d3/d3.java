package js.d3;

import elemental2.promise.Promise;
import js.d3.drag.DragBehavior;
import js.d3.force.ForceCenter;
import js.d3.force.ForceLink;
import js.d3.force.ForceManyBody;
import js.d3.force.Simulation;
import js.d3.hierarchy.ClusterLayout;
import js.d3.hierarchy.HierarchyNode;
import js.d3.scale.ScaleOrdinal;
import js.d3.scale.linear.ScaleLinear;
import js.d3.selection.Selection;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;
import jsinterop.base.Any;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "d3")
public class d3 {

    public static String[] schemeCategory10;

    public static Any event;

    public static native ClusterLayout cluster();

    public static native Selection selectAll(Object selector);

    public static native Selection select(Object selector);

    public static native ScaleLinear scaleLinear();

    public static native double max(double[] data);

    public static native <T> Promise<T> json(String url);

    public static native HierarchyNode hierarchy(Object data);

    public static native Simulation forceSimulation(Object[] nodesData);

    public static native ForceLink forceLink(Object[] links);

    public static native ForceManyBody forceManyBody();

    public static native ForceCenter forceCenter(int x, int y);

    public static native ScaleOrdinal scaleOrdinal(Object[] range);

    public static native <T> DragBehavior<T> drag();
}
