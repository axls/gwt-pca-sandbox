package js.d3;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.dom.client.StyleInjector;

import js.d3.resources.D3Resources;

public class D3EntryPoint implements EntryPoint {

    @Override
    public void onModuleLoad() {
        D3Resources resources = D3Resources.INSTANCE;
        StyleInjector.inject(resources.d3Styles().getText());
        if (isDevMode()) {
            ScriptInjector.fromString(resources.d3Js().getText())
                    .setWindow(ScriptInjector.TOP_WINDOW)
                    .setRemoveTag(false)
                    .inject();
        } else {
            ScriptInjector.fromString(resources.d3MinJs().getText())
                    .setWindow(ScriptInjector.TOP_WINDOW)
                    .inject();
        }
    }

    private static boolean isDevMode() {
        return "on".equals(System.getProperty("superdevmode", "off"));
    }
}
