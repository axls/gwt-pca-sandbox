package js.d3.scale.linear;

import js.d3.common.JsFunction;
import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public class ScaleLinear {

    public native double[] domain();

    public native ScaleLinear domain(double[] domain);

    public native double[] range();

    public native ScaleLinear range(double[] range);

    @JsOverlay
    public final <T, R> R invoke(T data) {
        return Js.<JsFunction<T, R>> cast(this).apply(data);
    };
}
