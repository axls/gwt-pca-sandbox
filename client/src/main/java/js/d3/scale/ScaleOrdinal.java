package js.d3.scale;

import js.d3.common.JsFunction;
import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public class ScaleOrdinal {

    @JsOverlay
    public final <T, R> R invoke(T data) {
        return Js.<JsFunction<T, R>> cast(this).apply(data);
    };
}
