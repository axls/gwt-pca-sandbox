package pca.sandbox.client;

import com.axellience.vuegwt.core.client.component.options.CustomizeOptions;
import com.axellience.vuegwt.core.client.component.options.VueComponentOptions;
import com.axellience.vueroutergwt.client.RouteConfig;
import com.axellience.vueroutergwt.client.RouterOptions;
import com.axellience.vueroutergwt.client.VueRouter;

import pca.sandbox.client.pages.graph.GraphsPageFactory;
import pca.sandbox.client.pages.pca.PcaPageFactory;

public class RoutesConfig implements CustomizeOptions {

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
	public void customizeOptions(VueComponentOptions vueComponentOptions) {
		RouterOptions routerOptions = new RouterOptions();
		addRoutes(routerOptions);
		
        vueComponentOptions.set("router", new VueRouter(routerOptions));
	}

	private void addRoutes(RouterOptions routerOptions) {
		routerOptions
			.addRoute("/pca", PcaPageFactory.get())
			.addRoute("/graphs", GraphsPageFactory.get())
			.addRoute(new RouteConfig().setPath("/").setRedirect("/graphs"));
	}
}
