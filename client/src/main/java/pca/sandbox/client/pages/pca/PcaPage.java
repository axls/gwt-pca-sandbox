package pca.sandbox.client.pages.pca;

import static elemental2.dom.DomGlobal.*;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import elemental2.core.Global;
import elemental2.dom.Element;
import elemental2.dom.File;
import elemental2.dom.FileList;
import elemental2.dom.FileReader;
import elemental2.dom.HTMLInputElement;
import elemental2.dom.MouseEvent;
import elemental2.dom.MouseEventInit;
import jsinterop.annotations.JsMethod;
import jsinterop.base.Js;
import pca.sandbox.client.components.MatrixModel;
import pca.sandbox.client.resources.PcaResources;
import pca.sandbox.math.Matrix;
import pca.sandbox.math.transformations.pca.PcaTransformation;
import pca.sandbox.math.transformations.pca.PcaTransformationResult;

@Component
public class PcaPage implements IsVueComponent, HasCreated{
	@Data MatrixModel data = new MatrixModel();
	@Data MatrixModel scores = new MatrixModel();
	@Data MatrixModel payloads = new MatrixModel();
	@Data int numberOfPca = 12;
	@Data boolean autoscale = true;
	private HTMLInputElement fileInput;
	private String dataFilename = "peopleData.csv";
	
	@Override
	public void created() {
		String csv = PcaResources.INSTANCE.peopleData().getText();
		loadFromCsv(csv);
	}
	
	@JsMethod
	void loadFromFile() {
		fileInput = (HTMLInputElement) document.createElement("input");
		fileInput.type = "file";
		fileInput.accept = "text/csv";
		fileInput.style.display = "none";
		fileInput.onchange = e ->{
			HTMLInputElement target = (HTMLInputElement) e.target;
			FileList files = target.files;
			if(files.length < 1) return null;
			
			File file = files.getAt(0);
			dataFilename = file.name;
			FileReader reader = new FileReader();
			reader.onload = re -> {
				String csv = Js.asPropertyMap(re.target).getAny("result").asString();
				loadFromCsv(csv);
				fileInput.remove();
				fileInput = null;
				return null;
			};
			reader.readAsText(file);
			return null;
		};
		document.body.appendChild(fileInput);
		MouseEventInit eventInitDict = MouseEventInit.create();
		MouseEvent event = new MouseEvent("click", eventInitDict );
		fileInput.dispatchEvent(event);
	}

	private void loadFromCsv(String csv) {
		csv = csv.replace("\r\n", "\n");
		MatrixModel newModel = MatrixModel.fromCsv(csv);
		data = newModel;
		scores = new MatrixModel();
		payloads = new MatrixModel();
	}

	@JsMethod
	void saveToFiles() {
		String fileName = dataFilename.substring(0, dataFilename.indexOf('.'));
		saveToFile(scores, fileName + "-scores.csv");
		saveToFile(payloads, fileName + "-payloads.csv");
	}
	
	private void saveToFile(MatrixModel model, String filename) {
		Element pom = document.createElement("a");
		StringBuilder csv = new StringBuilder();
		for (int i = 0; i < model.colHeaders.length; i++) {
			String header = model.colHeaders[i];
			csv.append(";" + header);
		}
		csv.append("\n");
		
		for (int i = 0; i < model.data.length; i++) {
			csv.append(model.rowHeaders[i]);
			double[] row = model.data[i];
			for (int j = 0; j < row.length; j++) {
				double d = row[j];
				csv.append(";").append(d);
			}
			csv.append("\n");
		}
        pom.setAttribute("href", "data:text/csv;charset=utf-8," + Global.encodeURIComponent(csv.toString()));
        pom.setAttribute("download", filename);
        pom.click();
	}

	@JsMethod
	void calculate() {
		PcaTransformation transformation = new PcaTransformation();
		
		Matrix x = Matrix.of(data.data);
		if(autoscale) {
			x = x.autoscale();
		}
		String[] pcaHeaders = new String[numberOfPca];
		for(int i=0; i < numberOfPca; i++) {
			pcaHeaders[i] = "PC" + (i + 1);
		}
		
		PcaTransformationResult transform = transformation.transform(x , numberOfPca);
		Matrix scores = transform.getScores();
		MatrixModel scoresModel = new MatrixModel();
		scoresModel.colHeaders = pcaHeaders;
		scoresModel.data = scores.data();
		scoresModel.rowHeaders = data.rowHeaders;
		this.scores = scoresModel;
		
		Matrix payloads = transform.getPayloads();
		MatrixModel payloadsModel = new MatrixModel();
		payloadsModel.colHeaders = pcaHeaders;
		payloadsModel.data = payloads.transpose().data();
		payloadsModel.rowHeaders = data.colHeaders;
		this.payloads = payloadsModel;
	}
	
	@JsMethod
	String dataHeader() {
		String result = "Данные";
		if(dataFilename  != null) {
			result += " - " + dataFilename;
		}
		return result ;
	}

}
