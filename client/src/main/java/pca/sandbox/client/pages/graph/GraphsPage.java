package pca.sandbox.client.pages.graph;

import static elemental2.dom.DomGlobal.*;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

import elemental2.dom.File;
import elemental2.dom.FileList;
import elemental2.dom.FileReader;
import elemental2.dom.FileReader.OnloadFn;
import elemental2.dom.HTMLInputElement;
import elemental2.dom.MouseEvent;
import elemental2.dom.MouseEventInit;
import elemental2.dom.ProgressEvent;
import jsinterop.annotations.JsMethod;
import jsinterop.base.Js;
import pca.sandbox.client.components.DendrogramComponentModel;
import pca.sandbox.client.components.GraphComponentModel;
import pca.sandbox.client.components.MatrixModel;
import pca.sandbox.client.resources.PcaResources;
import pca.sandbox.math.HCluster;
import pca.sandbox.math.Matrix;
import pca.sandbox.math.transformations.pca.PcaTransformation;
import pca.sandbox.math.transformations.pca.PcaTransformationResult;

@Component
public class GraphsPage implements IsVueComponent, HasCreated {
    @Data
    MatrixModel data = new MatrixModel();
    @Data
    MatrixModel scores = new MatrixModel();
    @Data
    GraphComponentModel graphModel = new GraphComponentModel();
    @Data
    DendrogramComponentModel dendrogramModel = new DendrogramComponentModel();
    private HTMLInputElement fileInput;
    private String dataFilename = "task2.csv";

    @Override
    public void created() {
        String csv = PcaResources.INSTANCE.task2Data().getText();
        loadFromCsv(csv);
    }

    @JsMethod
    String dataHeader() {
        String result = "Исходные данные";
        if (dataFilename != null) {
            result += " - " + dataFilename;
        }
        return result;
    }

    @JsMethod
    void loadFromFile() {
        fileInput = (HTMLInputElement) document.createElement("input");
        fileInput.type = "file";
        fileInput.accept = "text/csv";
        fileInput.style.display = "none";
        fileInput.onchange = e -> {
            HTMLInputElement target = (HTMLInputElement) e.target;
            FileList files = target.files;
            if (files.length < 1) return null;

            File file = files.getAt(0);
            dataFilename = file.name;
            FileReader reader = new FileReader();
            reader.onload = new OnloadFn() {
                @Override
                public Object onInvoke(ProgressEvent re) {
                    String csv = Js.asPropertyMap(re.target).getAny("result").asString();
                    loadFromCsv(csv);
                    fileInput.remove();
                    fileInput = null;
                    return null;
                }
            };
            reader.readAsText(file);
            return null;
        };
        document.body.appendChild(fileInput);
        MouseEventInit eventInitDict = MouseEventInit.create();
        MouseEvent event = new MouseEvent("click", eventInitDict);
        fileInput.dispatchEvent(event);
    }

    private void loadFromCsv(String csv) {
        data = MatrixModel.fromCsv(csv);
        String[] rowHeaders = data.rowHeaders;
        for (int i = 0; i < rowHeaders.length; i++) {
            rowHeaders[i] = rowHeaders[i] + "(" + (i + 1) + ")";
        }
        scores = new MatrixModel();

        PcaTransformation transformation = new PcaTransformation();
        Matrix m = Matrix.of(data.data);
        PcaTransformationResult transformationResult = transformation.transform(m, 1);
        scores = new MatrixModel();
        scores.data = transformationResult.getScores().data();
        scores.colHeaders = new String[] { "PC1" };

        HCluster cluster = HCluster.of(m);
        graphModel = GraphComponentModel.create(cluster);
        dendrogramModel = DendrogramComponentModel.create(cluster.getClusters().get(0));
    }
}
