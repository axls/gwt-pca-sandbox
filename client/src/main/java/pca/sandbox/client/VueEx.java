package pca.sandbox.client;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative=true, namespace=JsPackage.GLOBAL, name="Vue")
public abstract class VueEx {

	static native void use(Object library);
}
