package pca.sandbox.client.components;

import static java.lang.Math.*;

import java.util.Arrays;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;
import pca.sandbox.math.HCluster;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public class GraphComponentModel {

    @JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
    public static class GraphNodeModel {
        public String id;
        public String label;
        public int group;
        public int clusterNodeId;

        @JsOverlay
        public static GraphNodeModel create(String id, String label, int group, int clusterNodeId) {
            GraphNodeModel result = new GraphNodeModel();
            result.id = id;
            result.label = label;
            result.group = group;
            result.clusterNodeId = clusterNodeId;
            return result;
        }

        @JsOverlay
        public static GraphNodeModel create(String id, int group, int clusterNodeId) {
            return create(id, id, group, clusterNodeId);
        }

        @JsOverlay
        public static GraphNodeModel create(int clusterNodeId, int group) {
            return create(String.valueOf(clusterNodeId + 1), group, clusterNodeId);
        }

        @JsOverlay
        @Override
        public final String toString() {
            return "GraphNodeDto [id=" + id + ", group=" + group + "]";
        }
    }

    @JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
    public static class GraphLinkModel {
        public String source;
        public String target;
        public int value;

        @JsOverlay
        public static GraphLinkModel create(String source, String target) {
            return create(source, target, 1);
        }

        @JsOverlay
        public static GraphLinkModel create(String source, String target, int value) {
            GraphLinkModel result = new GraphLinkModel();
            result.source = source;
            result.target = target;
            result.value = value;
            return result;
        }

        @JsOverlay
        @Override
        public final String toString() {
            return "GraphLinkDto [source=" + source + ", target=" + target + ", value=" + value + "]";
        }
    }

    public GraphNodeModel[] nodes;
    public GraphLinkModel[] links;

    @JsOverlay
    public static GraphComponentModel create(HCluster cluster) {
        return new GraphModelBuilder(cluster).build();
    }

    @JsOverlay
    @Override
    public final String toString() {
        return "GraphDto [nodes=" + Arrays.toString(nodes) + ", links=" + Arrays.toString(links) + "]";
    }
}
