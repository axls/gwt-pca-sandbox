package pca.sandbox.client.components;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasRender;
import com.axellience.vuegwt.core.client.vnode.VNode;
import com.axellience.vuegwt.core.client.vnode.VNodeData;
import com.axellience.vuegwt.core.client.vnode.builder.VNodeBuilder;
import com.google.gwt.core.client.Scheduler;

import elemental2.core.JsArray;
import elemental2.core.JsObject;
import elemental2.dom.Element;
import js.d3.d3;
import js.d3.drag.D3DragEvent;
import js.d3.force.Simulation;
import js.d3.force.SimulationNode;
import js.d3.hierarchy.HierarchyPointLink;
import js.d3.selection.Selection;
import jsinterop.base.Any;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;
import pca.sandbox.client.components.GraphComponentModel.GraphNodeModel;

@Component(hasTemplate = false)
public class GraphComponent implements IsVueComponent, HasRender {

    @Ref
    public Element svg;
    @Prop
    GraphComponentModel model = new GraphComponentModel();

    @Override
    public VNode render(VNodeBuilder builder) {
        return builder.el("svg", VNodeData.get().setRef("svg"));
    }

    @Watch(value = "model", isImmediate = true)
    public void onModelChanged(GraphComponentModel data) {
        Scheduler.get().scheduleDeferred(() -> onDataLoaded(data));
    }

    private void onDataLoaded(GraphComponentModel data) {
        this.svg.innerHTML = "";
        JsPropertyMap<Object> dataPropetyMap = Js.asPropertyMap(data);
        JsObject[] links = dataPropetyMap.getAny("links").<JsArray<Any>> cast()
                .map((d, i, a) -> JsObject.create(d));
        JsObject[] nodes = dataPropetyMap.getAny("nodes").<JsArray<Any>> cast()
                .map((d, i, a) -> JsObject.create(d));

        int width = 900;
        int height = 600;

        Simulation simulation = d3.forceSimulation(nodes)
                .force("link", d3.forceLink(links).distance(50).id((GraphNodeModel d) -> d.id))
                .force("charge", d3.forceManyBody())
                .force("center", d3.forceCenter(width / 2, height / 2));

        Selection svg = d3.select(this.svg)
                .attr("width", width)
                .attr("height", height);
        Selection link = svg.append("g")
                .attr("stroke", "#999")
                .attr("stroke-opacity", 0.8)
                .selectAll("line")
                .data(links)
                .join("line")
                .attr("stroke-width", d -> 1);

        Selection node = svg.append("g")
                .attr("stroke", "#fff")
                .attr("stroke-width", 1.5)
                .selectAll("g")
                .data(nodes)
                .join("g")
                .call(d3.<SimulationNode> drag().on("start", d -> {
                    D3DragEvent event = d3.event.<D3DragEvent> cast();
                    if (event.active == 0) {
                        simulation.alphaTarget(0.3).restart();
                    }

                    d.fx = (double) d.x;
                    d.fy = (double) d.y;
                }).on("drag", d -> {
                    D3DragEvent event = d3.event.<D3DragEvent> cast();
                    d.fx = (double) event.x;
                    d.fy = (double) event.y;
                }).on("end", d -> {
                    D3DragEvent event = d3.event.<D3DragEvent> cast();
                    if (event.active == 0) {
                        simulation.alphaTarget(0);
                    }
                    d.fx = null;
                    d.fy = null;
                }));

        node.append("circle")
                .attr("r", 18)
                .attr("fill", (GraphNodeModel d) -> d.group == 1 ? "#1f77b4" : "#ff7f0e");
        node.append("text")
                .text((GraphNodeModel d) -> d.label)
                .attr("dy", 3)
                .attr("text-anchor", "middle")
                .attr("stroke", "white")
                .attr("font-size", "12");
        node.append("title")
                .text((GraphNodeModel d) -> d.label);

        simulation.on("tick", () -> {
            link
                    .attr("x1", (HierarchyPointLink d) -> d.source.x)
                    .attr("y1", (HierarchyPointLink d) -> d.source.y)
                    .attr("x2", (HierarchyPointLink d) -> d.target.x)
                    .attr("y2", (HierarchyPointLink d) -> d.target.y);
            node
                    .attr("transform", d -> "translate(" + Js.asPropertyMap(d).get("x") + "," + Js.asPropertyMap(d).get("y") + ")");
        });
    }
}
