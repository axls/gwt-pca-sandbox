package pca.sandbox.client.components;

import static java.lang.Math.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pca.sandbox.client.components.GraphComponentModel.GraphLinkModel;
import pca.sandbox.client.components.GraphComponentModel.GraphNodeModel;
import pca.sandbox.math.ClusterNode;
import pca.sandbox.math.HCluster;

class GraphModelBuilder {

    private static final double NEAR_ZERO = 1e-9;
    private final HCluster cluster;
    private final List<GraphNodeModel> nodes = new ArrayList<>();
    private final List<GraphLinkModel> links = new ArrayList<>();
    private Map<String, String> nodeRemap = new HashMap<>();

    GraphModelBuilder(HCluster cluster) {
        this.cluster = cluster;
    }

    GraphComponentModel build() {
        traverseClusters(cluster.getClusters().get(0));
        remapNodesInLinks();
        GraphComponentModel model = new GraphComponentModel();
        model.nodes = nodes.toArray(new GraphNodeModel[0]);
        model.links = links.toArray(new GraphLinkModel[0]);
        return model;
    }

    private void traverseClusters(ClusterNode cluster) {
        if (cluster.isLeaf()) return;

        List<ClusterNode> clusters = cluster.getChildren();
        List<ClusterNode> leaves = new ArrayList<>();
        for (ClusterNode subCluster : clusters) {
            if (subCluster.isLeaf()) {
                leaves.add(subCluster);
            } else {
                int[] closest = findClosest(subCluster, clusters);
                if (closest[0] != -1) {
                    links.add(GraphLinkModel.create(String.valueOf(closest[0] + 1),
                            String.valueOf(closest[1] + 1), 1));
                }
                traverseClusters(subCluster);
            }
        }
        boolean hasAggregates = cluster.getValue() < NEAR_ZERO;
        if (leaves.size() == 2 && hasAggregates) {
            int firstNodeId = leaves.get(0).getPayload().get(0);
            int secondNodeId = leaves.get(1).getPayload().get(0);
            int nodeId = min(firstNodeId, secondNodeId);
            nodeRemap.put(String.valueOf(firstNodeId + 1), String.valueOf(nodeId + 1));
            nodeRemap.put(String.valueOf(secondNodeId + 1), String.valueOf(nodeId + 1));
            GraphNodeModel graphNode = GraphNodeModel.create(
                    String.valueOf(nodeId + 1),
                    String.valueOf(firstNodeId + 1) + "/" + String.valueOf(secondNodeId + 1), 2,
                    firstNodeId);
            this.nodes.add(graphNode);
        } else {
            GraphNodeModel prevNode = null;
            GraphNodeModel firstNode = null;
            for (ClusterNode leaf : leaves) {
                GraphNodeModel graphNode = GraphNodeModel.create(leaf.getPayload().get(0), hasAggregates ? 2 : 1);
                this.nodes.add(graphNode);
                if (firstNode == null) {
                    firstNode = graphNode;
                }
                if (prevNode != null) {
                    links.add(GraphLinkModel.create(prevNode.id, graphNode.id));
                }
                prevNode = graphNode;
            }
            if (firstNode != null && firstNode != prevNode) {
                links.add(GraphLinkModel.create(prevNode.id, firstNode.id));
            }
        }
    }

    private void remapNodesInLinks() {
        for (GraphLinkModel link : links) {
            link.source = nodeRemap.getOrDefault(link.source, link.source);
            link.target = nodeRemap.getOrDefault(link.target, link.target);
        }
    }

    private int[] findClosest(ClusterNode cluster, List<ClusterNode> clusters) {
        int[] result = { -1, -1 };
        double distance = Double.MAX_VALUE;
        for (int node : cluster.getPayload()) {
            for (ClusterNode otherCluster : clusters) {
                for (int otherNode : otherCluster.getPayload()) {
                    if (cluster.getPayload().contains(otherNode)) continue;
                    if (this.cluster.getDists()[node][otherNode] < distance) {
                        distance = this.cluster.getDists()[node][otherNode];
                        result[0] = node;
                        result[1] = otherNode;
                    }
                }
            }
        }
        return result;
    }

}