package pca.sandbox.client.components;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasBeforeUpdate;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasUpdated;
import com.google.gwt.i18n.client.NumberFormat;

import jsinterop.annotations.JsMethod;

@Component
public class MatrixComponent implements IsVueComponent, HasCreated, HasUpdated, HasBeforeUpdate {
	
	private static final NumberFormat decimalFormat = NumberFormat.getDecimalFormat();
	
	@Prop
	MatrixModel model = new MatrixModel();
	@Prop
	String header = "";
	@Prop String showLambdas;
	
	@Override
	public void created() {
	}
	
	@Override
	public void beforeUpdate() {
	}
	
	@Override
	public void updated() {
	}
	
	@JsMethod
	String value(int i, int j) {
		return decimalFormat.format(model.data[i][j]);
	}
	
	@JsMethod
	String lambda(int i) {
		return decimalFormat.format(model.getLambdas()[i]);
	}
	
	@JsMethod
	String lambdaTotal() {
		return decimalFormat.format(model.getLambdaTotal());
	}
	
	@JsMethod
	public boolean isShowLambdas() {
		return "true".equals(showLambdas);
	}
}
