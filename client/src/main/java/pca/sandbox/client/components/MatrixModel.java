package pca.sandbox.client.components;

import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.i18n.client.constants.NumberConstants;

import pca.sandbox.math.Matrix;

public class MatrixModel {
	public double [][] data;
	public String[] rowHeaders;
	public String[] colHeaders;
	private double[] lambdas;

	public MatrixModel() {
		data = new double[1][1];
		rowHeaders = new String[] {""};
		colHeaders = new String[] {""};
	}
	
	int getRowNumber() {
		return data.length;
	}
	
	int getColNumber() {
		return data.length == 0 ? 0 : data[0].length;
	}

	double[] getLambdas() {
		if(data == null) return new double[0];
		if(lambdas == null) {
			Matrix matrix = Matrix.of(data);
			lambdas = matrix.lambdas().firstRow();
		}
		
		return lambdas;
	}
	public static MatrixModel fromCsv(String csv) {
		csv = csv.replace("\r\n", "\n");
		MatrixModel model = new MatrixModel();
		if(csv == null || csv.isEmpty()) return model;
		
		String[] rows = csv.split("\n");
		String headerRow = rows[0];
		
		String[] headers = headerRow.split(";");
		model.colHeaders = new String[headers.length - 1];
		System.arraycopy(headers, 1, model.colHeaders, 0, model.colHeaders.length);

		model.data = new double[rows.length - 1][headers.length - 1];
		model.rowHeaders = new String[rows.length -1 ];
		
		for (int i = 1; i < rows.length; i++) {
			String row = rows[i];
			String[] cols = row.split(";");
			model.rowHeaders[i-1] = cols[0];
			for (int j = 1; j < cols.length; j++) {
				String col = cols[j];
				col = normalizeDecimalNumber(col);
				double colValue = NumberFormat.getDecimalFormat().parse(col);
				model.data[i-1][j-1] = colValue;
			}
			
		}
		
		return model;
	}

	private static String normalizeDecimalNumber(String number) {
		NumberConstants numberConstants = LocaleInfo.getCurrentLocale().getNumberConstants();
		if(".".equals(numberConstants.decimalSeparator())) {
			number = number.replace(',', '.');
		}else {
			number = number.replace('.', ',');
		}
		return number;
	}

	Number getLambdaTotal() {
		double total = 0;
		for (double l : getLambdas()) {
			total += l;
		}
		return total;
	}
}
