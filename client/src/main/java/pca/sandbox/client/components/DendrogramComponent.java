package pca.sandbox.client.components;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasRender;
import com.axellience.vuegwt.core.client.vnode.VNode;
import com.axellience.vuegwt.core.client.vnode.VNodeData;
import com.axellience.vuegwt.core.client.vnode.builder.VNodeBuilder;
import com.google.gwt.core.client.Scheduler;

import elemental2.dom.Element;
import js.d3.d3;
import js.d3.hierarchy.HierarchyNode;
import js.d3.hierarchy.HierarchyPointNode;
import js.d3.selection.Selection;
import jsinterop.base.Js;

@Component(hasTemplate = false)
public class DendrogramComponent implements IsVueComponent, HasRender {

    @Ref
    public Element svg;
    @Prop
    DendrogramComponentModel model = new DendrogramComponentModel();

    @Override
    public VNode render(VNodeBuilder builder) {
        return builder.el("svg", VNodeData.get().setRef("svg"));
    }

    @Watch(value = "model", isImmediate = true)
    public void onModelChanged(GraphComponentModel data) {
        Scheduler.get().scheduleDeferred(() -> onDataLoaded(data));
    }

    private void onDataLoaded(Object data) {
        this.svg.innerHTML = "";

        int width = 932;
        int height = 400;
        HierarchyNode dataRoot = d3.hierarchy(data).sort((a, b) -> {
            if (a.height == b.height) {
                String aName = Js.asPropertyMap(a.data).getAny("name").asString();
                String bName = Js.asPropertyMap(b.data).getAny("name").asString();
                return aName.compareTo(bName);
            } else {
                return a.height - b.height;
            }
        });

        dataRoot.dx = 20;
        dataRoot.dy = height / (dataRoot.height + 1);

        HierarchyPointNode root = d3.cluster()
                .nodeSize(new int[] { dataRoot.dx, dataRoot.dy }).invoke(dataRoot);

        int[] xs = new int[] { Integer.MAX_VALUE, Integer.MIN_VALUE };
        root.each(d -> {
            if (d.x > xs[1]) xs[1] = d.x;
            if (d.x < xs[0]) xs[0] = d.x;
        });

        Selection svg = d3.select(this.svg)
                .attr("viewBox", new int[] { -200, -20, width, height });

        svg.append("g")
                .attr("font-family", "sans-serif")
                .attr("font-size", 10)
                .attr("transform", "translate(80, 0)");

        svg.selectAll("path.link")
                .data(root.descendants().slice(1))
                .enter().append("path")
                .attr("class", "link")
                .attr("d", (HierarchyPointNode d, int i) -> "\n" +
                        "M" + d.parent.x + "," + d.parent.y + "\n" +
                        "H" + d.x + "V" + (d.y - 0) + "\n");

        Selection node = svg.selectAll("g.node")
                .data(Js.<Object[]> cast(root.descendants()))
                .enter().append("g")
                .attr("class", "node")
                .attr("transform", (HierarchyPointNode d, int i) -> "translate(" + d.x + "," + d.y + ")");

        node.append("circle")
                .attr("visibility", (HierarchyPointNode d, int i) -> d.children != null ? "hidden" : "visible")
                .attr("r", 2.5);

        node.append("text")
                .attr("dx", "-0.5em")
                .attr("y", (HierarchyPointNode d, int i) -> d.children != null ? -6 : 16)
                .text((HierarchyPointNode d, int i) -> Js.asPropertyMap(d.data).get("name"))
                .filter((HierarchyPointNode d, int i) -> d.children)
                .attr("text-anchor", "end")
                .clone(true).lower()
                .attr("stroke", "white");
    }
}
