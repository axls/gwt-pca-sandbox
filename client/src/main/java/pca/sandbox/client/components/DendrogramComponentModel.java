package pca.sandbox.client.components;

import java.util.stream.Collectors;

import com.google.gwt.i18n.client.NumberFormat;

import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;
import pca.sandbox.math.ClusterNode;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "Object")
public class DendrogramComponentModel {

    public String name;
    public DendrogramComponentModel[] children;

    @JsOverlay
    public static DendrogramComponentModel create(ClusterNode src) {
        DendrogramComponentModel result = new DendrogramComponentModel();
        if (src.isLeaf()) {
            result.name = String.valueOf(1 + src.getPayload().get(0));
        } else {
            result.name = "(" + NumberFormat.getDecimalFormat().format(src.getValue()) + ")";

            result.children = src.getChildren().stream()
                    .map(DendrogramComponentModel::create)
                    .collect(Collectors.toList()).toArray(new DendrogramComponentModel[0]);
        }

        return result;
    }
}
