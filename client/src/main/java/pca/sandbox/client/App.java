package pca.sandbox.client;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;

@Component(customizeOptions = RoutesConfig.class)
public class App implements IsVueComponent, HasCreated{
	
	@Override
	public void created() {
	}
}
