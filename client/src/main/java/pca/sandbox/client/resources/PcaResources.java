package pca.sandbox.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;

public interface PcaResources extends ClientBundle {

	static final PcaResources INSTANCE = GWT.create(PcaResources.class);

	@Source("peopleData.csv")
	TextResource peopleData();

	@Source("task2.csv")
	TextResource task2Data();
}
