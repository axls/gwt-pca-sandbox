package pca.sandbox.client;

import com.axellience.vuegwt.core.client.Vue;

import pca.sandbox.client.components.DendrogramComponentFactory;
import pca.sandbox.client.components.GraphComponentFactory;
import pca.sandbox.client.components.MatrixComponentFactory;

class VueComponentsRegistrator {
    static void register() {
        Vue.component("matrix", MatrixComponentFactory.get());
        Vue.component("dendrogram", DendrogramComponentFactory.get());
        Vue.component("graph", GraphComponentFactory.get());
    }
}
