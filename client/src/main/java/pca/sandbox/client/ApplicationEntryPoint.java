package pca.sandbox.client;

import com.axellience.vuegwt.core.client.Vue;
import com.axellience.vuegwt.core.client.VueGWT;
import com.axellience.vueroutergwt.client.VueRouter;
import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.ScriptInjector;

import elemental2.dom.DomGlobal;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

public class ApplicationEntryPoint implements EntryPoint {

    @Override
    public void onModuleLoad() {
        VueGWT.init();
        VueRouter.init();
        ScriptInjector.fromUrl("/vue/vue-material.js").setWindow(ScriptInjector.TOP_WINDOW)
        .setCallback(new Callback<Void, Exception>() {
			@Override
			public void onSuccess(Void result) {
				init();
			}
			
			@Override
			public void onFailure(Exception reason) {
			}
		})
        .inject();
    }

	protected void init() {
		JsPropertyMap<Object> wnd = Js.cast(DomGlobal.window);
		Object vueMaterial = wnd.getAny("VueMaterial").asPropertyMap().get("default");
		VueEx.use(vueMaterial);
        VueComponentsRegistrator.register();
		Vue.attach("#app", AppFactory.get());
	}
}
