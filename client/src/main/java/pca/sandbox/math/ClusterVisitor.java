package pca.sandbox.math;

public interface ClusterVisitor {

    void visitNode(ClusterNode node);
}
