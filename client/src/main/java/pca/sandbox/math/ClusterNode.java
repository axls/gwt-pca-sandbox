package pca.sandbox.math;

import static java.util.stream.Collectors.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

public class ClusterNode {
    private List<Integer> payload = new ArrayList<>();
    private final double value;
    private final List<ClusterNode> children = new ArrayList<>();

    public ClusterNode(double value) {
        this.value = value;
    }

    public ClusterNode(double value, int[] payload) {
        this.value = value;
        this.payload.addAll(IntStream.of(payload).boxed().collect(toList()));
    }

    public List<ClusterNode> getChildren() {
        return Collections.unmodifiableList(children);
    }

    public List<Integer> getPayload() {
        return Collections.unmodifiableList(payload);
    }

    public void setPayload(int[] payload) {
        this.payload.clear();
        this.payload.addAll(IntStream.of(payload).boxed().collect(toList()));
    }

    public double getValue() {
        return value;
    }

    public boolean isLeaf() {
        return children.isEmpty();
    }

    public void add(ClusterNode child) {
        children.add(child);
        payload.addAll(child.getPayload());
    }

    public void addAll(Collection<? extends ClusterNode> nodes) {
        nodes.forEach(this::add);
    }

    public void remove(ClusterNode child) {
        children.remove(child);
        payload.removeAll(child.getPayload());
    }

    public void visit(ClusterVisitor visitor) {
        visitor.visitNode(this);
        children.forEach(c -> c.visit(visitor));
    }

    public boolean isAgregate() {
        if (value > 1e-9 || children.size() != 2) return false;
        return children.get(0).isLeaf() && children.get(1).isLeaf();
    }

    public boolean isMultiAgregate() {
        if (value > 1e-9 || children.size() < 2) return false;

        return isAllSubclustersAreLeaves();
    }

    public boolean isAllSubclustersAreLeaves() {
        for (ClusterNode subCluster : children) {
            if (!subCluster.isLeaf()) return false;
        }

        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<" + payload);
        if (!children.isEmpty()) {
            sb.append(", children=" + children);
        }
        sb.append(">");
        return sb.toString();
    }

    public int getFirstPayload() {
        return payload.get(0);
    }

    public boolean hasLeaves() {
        if(isLeaf()) return false;
        
        return children.stream().filter(ClusterNode::isLeaf).findFirst().isPresent();
    }

    public List<ClusterNode> getLeaves() {
        return children.stream().filter(ClusterNode::isLeaf).collect(toList());
    }
}