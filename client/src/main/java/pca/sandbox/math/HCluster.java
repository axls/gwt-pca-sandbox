package pca.sandbox.math;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HCluster {

    private static class ClosestResult {
        private List<ClusterNode> clusters;
        private double distance;

        ClosestResult(List<ClusterNode> clusters, double distance) {
            this.clusters = clusters;
            this.distance = distance;
        }

        @Override
        public String toString() {
            return "ClosestResult [distance=" + distance + ", clusters=" + clusters + "]";
        }
    }

    private List<ClusterNode> clusters = new ArrayList<>();
    private double[][] dists;

    public static HCluster of(Matrix m) {
        HCluster cluster = new HCluster();
        cluster.buildFrom(m);
        return cluster;
    }

    public List<ClusterNode> getClusters() {
        return clusters;
    }

    public double[][] getDists() {
        return dists;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        buildToString(sb, clusters, 1);
        return sb.toString();
    }

    private void buildToString(StringBuilder sb, List<ClusterNode> clusters, int level) {
        if (clusters.isEmpty()) return;
        sb.append(level + ":");
        for (ClusterNode node : clusters) {
            sb.append(" " + node.getPayload());
        }
        sb.append("\n");
        for (ClusterNode node : clusters) {
            buildToString(sb, node.getChildren(), level + 1);
        }
    }

    private void buildFrom(Matrix m) {
        dists = m.dist().data();
        clusters = new ArrayList<>();
        for (int i = 0; i < dists.length; i++) {
            ClusterNode node = new ClusterNode(0, new int[] { i });
            clusters.add(node);
        }

        while (clusters.size() > 1) {
            List<ClusterNode> newClusters = new ArrayList<>();
            Map<ClusterNode, ClusterNode> nodeCluster = new HashMap<>();
            for (int i = 0; i < clusters.size(); i++) {
                ClusterNode clusterNode = clusters.get(i);
                ClosestResult closestResult = findClosestFor(clusterNode, clusters.subList(i + 1, clusters.size()));

                ClusterNode nodeInCluster = nodeCluster.get(clusterNode);
                if (nodeInCluster != null) {
                    if (nodeInCluster.getValue() < closestResult.distance) {
                        continue;
                    }
                }

                ClusterNode newCluster = new ClusterNode(closestResult.distance);

                List<ClusterNode> nodes = new ArrayList<>();
                nodes.add(clusterNode);
                nodes.addAll(closestResult.clusters);
                for (ClusterNode node : nodes) {
                    nodeInCluster = nodeCluster.get(node);
                    if (nodeInCluster == null) {
                        newCluster.add(node);
                        nodeCluster.put(node, newCluster);
                    } else if (nodeInCluster != newCluster) {
                        if (newCluster.getValue() < nodeInCluster.getValue()) {
                            nodeInCluster.remove(node);
                            if (nodeInCluster.getChildren().isEmpty()) {
                                newClusters.remove(nodeInCluster);
                            }
                            newCluster.add(node);
                            nodeCluster.put(node, newCluster);
                        }
                    }
                }

                List<ClusterNode> newClusterNodes = newCluster.getChildren();
                if (newClusterNodes.isEmpty()) continue;
                if (newClusterNodes.size() == 1) {
                    newClusters.add(newClusterNodes.get(0));
                    continue;
                }
                newClusters.add(newCluster);
            }

            clusters = newClusters;
        }
    }

    private ClosestResult findClosestFor(ClusterNode clusterNode, List<ClusterNode> clusters) {
        List<ClusterNode> result = new ArrayList<>();
        double dist = Double.MAX_VALUE;
        for (ClusterNode node : clusters) {
            if (node == clusterNode) continue;

            double d = getDistBetween(clusterNode, node);
            if (Math.abs(d - dist) < 1e-9) {
                result.add(node);
            } else if (d < dist) {
                result.clear();
                result.add(node);
                dist = d;
            }
        }

        return new ClosestResult(result, dist);
    }

    private double getDistBetween(ClusterNode clusterNode, ClusterNode node) {
        if (clusterNode.getPayload().equals(node.getPayload())) return 0;

        double result = Double.MAX_VALUE;

        for (int i : clusterNode.getPayload()) {
            for (int j : node.getPayload()) {
                double d = dists[i][j];
                if (d < result) {
                    result = d;
                }
            }
        }

        return result;
    }

    public void visit(ClusterVisitor visitor) {
        clusters.forEach(c -> c.visit(visitor));
    }
}
