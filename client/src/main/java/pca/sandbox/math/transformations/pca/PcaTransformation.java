package pca.sandbox.math.transformations.pca;

import pca.sandbox.math.Matrix;

public class PcaTransformation {

    private double iterationDistanceDelta = 1e-6;
    private int maxIterations = 1000;

    public PcaTransformationResult transform(Matrix x, int numerOfPca) {
        int[] iterations = new int[numerOfPca];
        Matrix p = Matrix.empty();
        Matrix t = Matrix.empty();
        Matrix t1 = Matrix.of(x.column(0));
        for (int pc = 0; pc < numerOfPca; pc++) {
            Matrix p1 = t1.transpose().multiply(x.divide(t1.scalar())).transpose();
            p1 = p1.divide(p1.norm());
            t1 = x.multiply(p1).divide(p1.norm());
            double d0 = t1.scalar();

            p1 = t1.transpose().multiply(x.divide(t1.scalar())).transpose();
            p1 = p1.divide(p1.norm());
            t1 = x.multiply(p1).divide(p1.norm());
            double d = t1.scalar();

            int i = 0;
            while (i++ < maxIterations && d - d0 > iterationDistanceDelta) {
                p1 = t1.transpose().multiply(x.divide(t1.scalar())).transpose();
                p1 = p1.divide(p1.norm());
                t1 = x.multiply(p1).divide(p1.norm());
                d = t1.scalar();

                p1 = t1.transpose().multiply(x.divide(t1.scalar())).transpose();
                p1 = p1.divide(p1.norm());
                t1 = x.multiply(p1).divide(p1.norm());
                d0 = t1.scalar();
            }
            iterations[pc] = i - 1;
            x = x.sub(t1.multiply(p1.transpose()));
            p = p.addRows(p1.transpose().data());
            t = t.addCols(t1.data());
        }

        return new PcaTransformationResult(t, p, iterations);
    }

    public double getIterationDistanceDelta() {
        return iterationDistanceDelta;
    }

    public void setIterationDistanceDelta(double iterationDistanceDelta) {
        this.iterationDistanceDelta = iterationDistanceDelta;
    }

    public int getMaxIterations() {
        return maxIterations;
    }

    public void setMaxIterations(int maxIterations) {
        this.maxIterations = maxIterations;
    }
}
