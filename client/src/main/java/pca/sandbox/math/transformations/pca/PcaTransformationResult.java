package pca.sandbox.math.transformations.pca;

import pca.sandbox.math.Matrix;

public class PcaTransformationResult {

	private final Matrix scores;
	private final Matrix payloads;
	private final int[] iterations;

	PcaTransformationResult(Matrix scores, Matrix payloads, int[] iterations) {
		this.scores = scores;
		this.payloads = payloads;
		this.iterations = iterations;
	}
	
	public Matrix getScores() {
		return scores;
	}
	
	public Matrix getPayloads() {
		return payloads;
	}
	
	public int[] getIterations() {
		return iterations;
	}
}
