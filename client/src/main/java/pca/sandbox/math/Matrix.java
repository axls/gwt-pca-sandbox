package pca.sandbox.math;

import static java.lang.Math.*;

import java.util.Random;

public class Matrix {
	private static final Random RND = new Random();

	private final double[][] d;

	private Matrix(double[][] d) {
		this.d = d;
	}

	public static Matrix empty() {
		return new Matrix(new double[0][0]);
	}

	public static Matrix of(double[][] d) {
		if (d == null) {
			throw new NullPointerException("Data array must not be null");
		}
		checkRowSizes(d);
		return new Matrix(d);
	}

	private static void checkRowSizes(double[][] d) {
		if (d.length > 0) {
			int firstRowSize = d[0].length;

			for (int i = 1; i < d.length; i++) {
				if (d[i].length != firstRowSize) {
					throw new IllegalArgumentException("All rows must have the same size");
				}
			}
		}
	}

	public static Matrix of(int r, int c) {
		return new Matrix(new double[r][c]);
	}

	public static Matrix rVector(int r) {
		return of(r, 1);
	}

	public static Matrix cVector(int c) {
		return of(1, c);
	}

	public static Matrix ofOnes(int r, int c) {
		Matrix matrix = new Matrix(new double[r][c]);
		matrix.fill(1);
		return matrix;
	}

	public static Matrix copyOf(Matrix src) {
		if (src.isEmpty())
			return Matrix.empty();

		return new Matrix(src.data());
	}

	public static Matrix loadFromCsv(String csv, char decimalDelimiter, boolean skipFirstLine, boolean skipFirstColumn) {
		String[] rows = csv.split("\n");
		String headerRow = rows[0];
		
		String[] headers = headerRow.split(";");
		
		int rowOffset = skipFirstLine ? 1 : 0;
		int colOffset = skipFirstColumn ? 1 : 0;
		double[][] d = new double[rows.length - rowOffset][headers.length - colOffset];
		
		for (int i = rowOffset; i < rows.length; i++) {
			String row = rows[i];
			String[] cols = row.split(";");
			for (int j = colOffset; j < cols.length; j++) {
				String col = cols[j];
				if(decimalDelimiter == '.') {
					col = col.replace(',', decimalDelimiter);						
				}else if(decimalDelimiter == ',') {
					col = col.replace('.', decimalDelimiter);
				}
				double colValue = Double.valueOf(col);
				d[i-rowOffset][j-colOffset] = colValue;
			}
		}
		
		return new Matrix(d);
	}
	
	public double get(int r, int c) {
		return d[r][c];
	}

	public double set(int r, int c, double newValue) {
		double oldValue = d[r][c];
		d[r][c] = newValue;
		return oldValue;
	}

	public Matrix fillRandom() {
		for (int i = 0; i < d.length; i++) {
			for (int j = 0; j < d[i].length; j++) {
				d[i][j] = RND.nextDouble();
			}
		}

		return this;
	}

	public int getRowsCount() {
		return d.length;
	}

	public int getColsCount() {
		return d.length == 0 ? 0 : d[0].length;
	}

	public double[][] data() {
		if (isEmpty())
			return d;

		double[][] d = new double[getRowsCount()][getColsCount()];
		for (int i = 0; i < d.length; i++) {
			for (int j = 0; j < d[i].length; j++) {
				d[i][j] = this.d[i][j];
			}
		}

		return d;
	}

	public double firstValue() {
		return d[0][0];
	}

	public double[] firstRow() {
		return d[0];
	}

	public double[][] firstColumn() {
		return column(0);
	}

	public double[] firstColumnAsArray() {
		return columnAsArray(0);
	}

	public double[] row(int index) {
		return d[index];
	}

	public double[][] column(int index) {
		double[][] c = new double[getRowsCount()][1];
		for (int i = 0; i < d.length; i++) {
			c[i][0] = d[i][index];
		}
		return c;
	}

	public double[] columnAsArray(int index) {
		double[] c = new double[getRowsCount()];
		for (int i = 0; i < d.length; i++) {
			c[i] = d[i][index];
		}
		return c;
	}

	public Matrix transpose() {
		double t[][] = new double[getColsCount()][getRowsCount()];

		for (int i = 0; i < d.length; i++) {
			for (int j = 0; j < d[i].length; j++) {
				t[j][i] = d[i][j];
			}
		}

		return new Matrix(t);
	}

	public Matrix multiply(Matrix m) {
		if (getColsCount() != m.getRowsCount()) {
			throw new IllegalArgumentException("Number of the columns of the left hand matrix"
					+ " and number of the rows of the right hand matrix should be equals");
		}

		double[][] md = m.d;
		double[][] r = new double[getRowsCount()][m.getColsCount()];
		for (int i = 0; i < d.length; i++) {
			for (int mc = 0; mc < m.getColsCount(); mc++) {
				double v = 0;
				for (int j = 0; j < d[i].length; j++) {
					v += d[i][j] * md[j][mc];
				}
				r[i][mc] = v;
			}
		}

		return new Matrix(r);
	}

	public Matrix add(Matrix m) {
		if (getColsCount() != m.getColsCount() || getRowsCount() != m.getRowsCount()) {
			throw new IllegalArgumentException("Matrices sizes should be equals");
		}

		double[][] md = m.d;
		double[][] r = new double[getRowsCount()][getColsCount()];
		for (int i = 0; i < d.length; i++) {
			for (int j = 0; j < d[i].length; j++) {
				r[i][j] = d[i][j] + md[i][j];
			}
		}

		return new Matrix(r);
	}

	public Matrix sub(Matrix m) {
		if (getColsCount() != m.getColsCount() || getRowsCount() != m.getRowsCount()) {
			throw new IllegalArgumentException("Matrices sizes should be equals");
		}

		double[][] md = m.d;
		double[][] r = new double[getRowsCount()][getColsCount()];
		for (int i = 0; i < d.length; i++) {
			for (int j = 0; j < d[i].length; j++) {
				r[i][j] = d[i][j] - md[i][j];
			}
		}

		return new Matrix(r);
	}

	public Matrix divide(double v) {
		double[][] d = data();
		for (int i = 0; i < d.length; i++) {
			for (int j = 0; j < d[i].length; j++) {
				d[i][j] = d[i][j] / v;
			}
		}

		return new Matrix(d);
	}

	public double scalar() {
		if (!isVector()) {
			throw new IllegalStateException("Norm may be calculated only for vector");
		}

		if (isCVector()) {
			return multiply(transpose()).firstValue();
		} else {
			return transpose().multiply(this).firstValue();
		}
	}

	public double norm() {
		return abs(sqrt(scalar()));
	}

	public boolean isRVector() {
		return getColsCount() == 1;
	}

	public boolean isCVector() {
		return getRowsCount() == 1;
	}

	public boolean isVector() {
		return isRVector() || isCVector();
	}

	public Matrix addRows(double[][] newRows) {
		checkRowSizes(newRows);

		if (isEmpty()) {
			return new Matrix(newRows);
		}

		if (newRows[0].length != getColsCount()) {
			throw new IllegalArgumentException("Number of columns in the appendable rows should be"
					+ " the same as number of the columns in this matrix");
		}

		double[][] newData = new double[d.length + newRows.length][getColsCount()];
		System.arraycopy(d, 0, newData, 0, d.length);
		System.arraycopy(newRows, 0, newData, d.length, newRows.length);

		return new Matrix(newData);
	}

	public Matrix addCols(double[][] newCols) {
		checkRowSizes(newCols);

		if (isEmpty()) {
			return new Matrix(newCols);
		}

		if (newCols.length != getRowsCount()) {
			throw new IllegalArgumentException("Number of rows in the appendable columns should be"
					+ " the same as number of the rows in this matrix");
		}

		double[][] newData = new double[d.length][getColsCount() + newCols[0].length];

		for (int i = 0; i < d.length; i++) {
			for (int j = 0; j < d[i].length; j++) {
				newData[i][j] = d[i][j];
			}
			for (int j = 0; j < newCols[i].length; j++) {
				newData[i][d[i].length + j] = newCols[i][j];
			}
		}

		return new Matrix(newData);
	}

	public boolean isEmpty() {
		return d.length == 0;
	}

	public Matrix mean() {
		double[][] result = new double[1][getColsCount()];
		for (int i = 0; i < getColsCount(); i++) {
			double mean = 0;
			for (int j = 0; j < getRowsCount(); j++) {
				mean += d[j][i];
			}
			result[0][i] = mean / getRowsCount();
		}

		return new Matrix(result);
	}

	public Matrix fill(double filler) {
		for (int i = 0; i < d.length; i++) {
			for (int j = 0; j < d[i].length; j++) {
				d[i][j] = filler;
			}
		}

		return this;
	}

	public Matrix center() {
		Matrix mean = mean();
		double[][] d = data();
		for (int i = 0; i < d.length; i++) {
			for (int j = 0; j < d[i].length; j++) {
				d[i][j] = d[i][j] - mean.get(0, j);
			}
		}

		return new Matrix(d);
	}

	public Matrix std() {
		return std(0);
	}
	
	public Matrix std(int weight) {
		Matrix mean = mean();
		double[][] result = new double[1][getColsCount()];
		for (int i = 0; i < getColsCount(); i++) {
			double std = 0;
			for (int j = 0; j < getRowsCount(); j++) {
				double s = d[j][i] - mean.d[0][i];
				std += s * s;
			}
			result[0][i] = sqrt(std / (getRowsCount() - (weight == 0 ? 1 : 0)) );
		}

		return new Matrix(result);
	}

	public Matrix autoscale() {
		return autoscale(0);
	}
	
	public Matrix autoscale(int weight) {
		Matrix std = std(weight);

		double[][] d = center().data();
		for (int i = 0; i < d.length; i++) {
			for (int j = 0; j < d[i].length; j++) {
				d[i][j] = d[i][j] / std.get(0, j);
			}
		}

		return new Matrix(d);
	}

	public Matrix lambdas() {
		if(isEmpty()) return empty();
		
		double l[] = new double[getColsCount()];
		for (int i = 0; i < d.length; i++) {
			for (int j = 0; j < d[i].length; j++) {
				l[j] += d[i][j] * d[i][j];
			}
		}
		
		return new Matrix(new double[][] {l});
	}

    public Matrix dist() {
        double[][] dists = new double[getRowsCount()][getRowsCount()];
        for (int i = 0; i < d.length; i++) {
            for (int j = 0; j < d.length; j++) {
                double dist = 0;
                for (int c = 0; c < d[i].length; c++) {
                    dist += (d[i][c] - d[j][c]) * (d[i][c] - d[j][c]);
                }
                dists[i][j] = sqrt(dist);
            }
        }

        return Matrix.of(dists);
    }
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Matrix " + getRowsCount() + "x" + getColsCount() + "\n");

		if (getColsCount() <= 100 && getRowsCount() <= 100) {
			for (int i = 0; i < d.length; i++) {
				sb.append("| ");
				for (int j = 0; j < d[i].length; j++) {
					sb.append(d[i][j]);
					if (j < d[i].length - 1) {
						sb.append(", ");
					}
				}
				sb.append(" |\n");
			}
		}

		return sb.toString();
	}
}
