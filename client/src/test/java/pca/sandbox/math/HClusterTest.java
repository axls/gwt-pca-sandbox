package pca.sandbox.math;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class HClusterTest {

    @Test
    public void test() throws IOException {
        String csv = IOUtils.toString(getClass().getResourceAsStream("/pca/sandbox/client/resources/task2.csv"));
        Matrix m = Matrix.loadFromCsv(csv, '.', true, true);

        System.out.println(m.dist());
        HCluster cluster = HCluster.of(m);
        System.out.println(cluster.toString());
    }

    @Test
    public void noDuplicates() throws IOException {
        String csv = IOUtils.toString(getClass().getResourceAsStream("/pca/sandbox/client/resources/task2.csv"));
        Matrix m = Matrix.loadFromCsv(csv, '.', true, true);

        System.out.println(m.dist());
        HCluster cluster = HCluster.of(m);
        System.out.println(cluster.toString());
        cluster.visit(new ClusterVisitor() {
            @Override
            public void visitNode(ClusterNode cluster) {
                Set<Integer> nodeIds = new HashSet<>();
                for (int id : cluster.getPayload()) {
                    if (nodeIds.contains(id)) {
                        fail("Найден узел-дубликат [" + id + "]. Кластер не должен содержать дубликаты узлов");
                    }
                    nodeIds.add(id);
                }

                nodeIds = new HashSet<>();
                for (ClusterNode subCluster : cluster.getChildren()) {
                    for (int id : subCluster.getPayload()) {
                        if (nodeIds.contains(id)) {
                            fail("Найден узел-дубликат [" + id + "]. Дочерние кластеры не должен содержать дубликаты узлов");
                        }
                        nodeIds.add(id);
                    }
                }
            }
        });
    }
}
