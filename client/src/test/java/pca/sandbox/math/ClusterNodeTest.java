package pca.sandbox.math;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class ClusterNodeTest {

    @Test
    public void add() {
        ClusterNode n = new ClusterNode(0, new int[] { 1, 2 });
        ClusterNode n1 = new ClusterNode(0, new int[] { 3, 4 });

        n.add(n1);
        assertThat(n.getPayload(), contains(1, 2, 3, 4));
    }

    @Test
    public void addAll() {
        ClusterNode n = new ClusterNode(0, new int[] { 1, 2 });
        ClusterNode n1 = new ClusterNode(0, new int[] { 3, 4 });
        ClusterNode n2 = new ClusterNode(0, new int[] { 5, 6 });

        n.addAll(Arrays.asList(n1, n2));
        assertThat(n.getPayload(), contains(1, 2, 3, 4, 5, 6));
    }

    @Test
    public void remove() {
        ClusterNode n = new ClusterNode(0, new int[] { 1, 2 });
        ClusterNode n1 = new ClusterNode(0, new int[] { 3, 4 });
        ClusterNode n2 = new ClusterNode(0, new int[] { 5, 6 });

        n.addAll(Arrays.asList(n1, n2));
        assertThat(n.getPayload(), contains(1, 2, 3, 4, 5, 6));

        n.remove(n1);
        assertThat(n.getPayload(), contains(1, 2, 5, 6));
    }
}
