package pca.sandbox.math;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class MatrixTest {

	private static final double DELTA = 0.001;
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@Test
	public void transpose() {
		Matrix m = Matrix.of(new double[][] {
			{1, 2, 3},
			{4, 5, 6},
		});
		
		Matrix mT = m.transpose();
		assertThat(mT.data(), is(new double[][] {
			{1, 4},
			{2, 5},
			{3, 6},
		}));

		m = Matrix.of(new double[][] {
			{1, 2, 3},
		});
		
		mT = m.transpose();
		assertThat(mT.data(), is(new double[][] {
			{1},
			{2},
			{3},
		}));

		m = Matrix.of(new double[][] {
			{1},
			{2},
			{3},
		});
		
		mT = m.transpose();
		assertThat(mT.data(), is(new double[][] {
			{1, 2, 3},
		}));
	}
	
	@Test
	public void multiply() {
		Matrix a = Matrix.of(new double[][] {
			{1, 2, 3},
			{4, 5, 6},
		});
		Matrix b = Matrix.of(new double[][] {
			{1, 4},
			{2, 5},
			{3, 6},
		});
		
		Matrix m = a.multiply(b);
		assertThat(m.data(), is(new double[][] {
			{14, 32},
			{32, 77},
		}));
	}
	
	@Test
	public void multiply_rVector_by_cVector() {
		Matrix a = Matrix.of(new double[][] {
			{1, 2, 3},
		});
		Matrix b = Matrix.of(new double[][] {
			{1},
			{2},
			{3},
		});
		
		Matrix m = a.multiply(b);
		assertThat(m.data(), is(new double[][] {
			{14},
		}));
	}

	@Test
	public void multiply_cVector_by_rVector() {
		Matrix a = Matrix.of(new double[][] {
			{1},
			{2},
			{3},
		});
		Matrix b = Matrix.of(new double[][] {
			{1, 2, 3},
		});
		
		Matrix m = a.multiply(b);
		assertThat(m.data(), is(new double[][] {
			{1, 2, 3},
			{2, 4, 6},
			{3, 6, 9},
		}));
	}
	
	@Test
	public void multiply_wrong_sizes() {
		Matrix a = Matrix.of(new double[][] {
			{1, 2, 3},
			{4, 5, 6},
		});
		Matrix b = Matrix.of(new double[][] {
			{1, 4},
			{2, 5},
		});
		
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("Number of the columns of the left hand matrix"
				+ " and number of the rows of the right hand matrix should be equals");
		a.multiply(b);
	}
	
	@Test
	public void norm() {
		Matrix a = Matrix.of(new double[][] {
			{1},
			{2},
			{2},
		});
		assertThat(a.norm(), is(3.0));

		a = Matrix.of(new double[][] {
			{1, 2, 2},
		});
		assertThat(a.norm(), is(3.0));
	}

	@Test
	public void addRows() {
		Matrix a = Matrix.of(new double[][] {
			{1, 2, 3},
		});
		a = a.addRows(new double[][] {
			{4, 5, 6},
		});
		assertThat(a.data(), is(new double[][] {
			{1, 2, 3},
			{4, 5, 6},
		}));
	}
	
	@Test
	public void addRowsToEmpty() {
		Matrix a = Matrix.empty();
		a = a.addRows(new double[][] {
			{1, 2, 3},
			{4, 5, 6},
		});
		assertThat(a.data(), is(new double[][] {
			{1, 2, 3},
			{4, 5, 6},
		}));
	}
	
	@Test
	public void addCols() {
		Matrix a = Matrix.of(new double[][] {
			{1},
			{2},
			{3},
		});
		a = a.addCols(new double[][] {
			{4},
			{5},
			{6},
		});
		assertThat(a.data(), is(new double[][] {
			{1, 4},
			{2, 5},
			{3, 6},
		}));
	}

	@Test
	public void addColsToEmpty() {
		Matrix a = Matrix.empty();
		a = a.addCols(new double[][] {
			{1, 4},
			{2, 5},
			{3, 6},
		});
		assertThat(a.data(), is(new double[][] {
			{1, 4},
			{2, 5},
			{3, 6},
		}));
	}
	
	@Test
	public void mean() {
		Matrix a = Matrix.of(new double[][] {
			{1, 4},
			{2, 6},
			{3, 8},
		});
		assertThat(a.mean().data(), is(new double[][] {
			{2, 6},
		}));
	}
	
	@Test
	public void std() {
		Matrix a = Matrix.of(new double[][] {
			{1, 4},
			{2, 6},
			{3, 8},
		});
		double[] std = a.std(1).data()[0];
		assertThat(std[0], closeTo(0.816, DELTA));
		assertThat(std[1], closeTo(1.633, DELTA));
	}
	
	@Test
	public void ofOnes() {
		assertThat(Matrix.ofOnes(2, 3).data(), is(new double[][] {
			{1, 1, 1},
			{1, 1, 1},
		}));
	}
	
	@Test
	public void center() {
		Matrix a = Matrix.of(new double[][] {
			{1, 4},
			{2, 6},
			{3, 8},
		});
		assertThat(a.center().data(), is(new double[][] {
			{-1, -2},
			{0, 0},
			{1, 2},
		}));

	}
	
	@Test
	public void autoscale() {
		Matrix a = Matrix.of(new double[][] {
			{1, 4},
			{2, 6},
			{3, 8},
		});
		Matrix scaled = a.autoscale();
		
		assertThat(scaled.mean().data(), is(new double[][] {
			{0.0, 0.0},
		}));
		
		double[] std = scaled.std().data()[0];
		assertThat(std[0], closeTo(1.0, DELTA));
		assertThat(std[1], closeTo(1.0, DELTA));
	}

	@Test
	public void copyOf() {
		Matrix a = Matrix.of(new double[][] {
			{1, 4},
			{2, 6},
			{3, 8},
		});
		assertThat(Matrix.copyOf(a).data(), is(new double[][] {
			{1, 4},
			{2, 6},
			{3, 8},
		}));
	}
	
	@Test
	public void loadFromCsv() {
		String csv = ";h1;h2;h3\n"
				+"c1;1;2;3\n"
				+"c1;4;5;6\n"
				+"c1;7;8;9\n"
				;
		
		Matrix a = Matrix.loadFromCsv(csv , '.', true, true);
		assertThat(a.data(), is(new double[][] {
			{1, 2, 3},
			{4, 5, 6},
			{7, 8, 9},
		}));
		
		csv = "c1;1;2;3\n"
				+"c1;4;5;6\n"
				+"c1;7;8;9\n"
				;
		
		a = Matrix.loadFromCsv(csv , '.', false, true);
		assertThat(a.data(), is(new double[][] {
			{1, 2, 3},
			{4, 5, 6},
			{7, 8, 9},
		}));
	}
	
	@Test
	public void loadFromCsv_skip_header_and_first_column() {
		String csv = "1;2;3\n"
				+"4;5;6\n"
				+"7;8;9\n"
				;
		
		Matrix a = Matrix.loadFromCsv(csv , '.', false, false);
		assertThat(a.data(), is(new double[][] {
			{1, 2, 3},
			{4, 5, 6},
			{7, 8, 9},
		}));
	}
	
	@Test
	public void lambdas() {
		Matrix a = Matrix.of(new double[][] {
			{1, 4},
			{2, 6},
			{3, 8},
		});
		assertThat(a.lambdas().data(), is(new double[][] {
                { 14, 116 },
		}));
	}

    @Test
    public void dist() throws IOException {
        Matrix a = Matrix.of(new double[][] {
                { 1, 2 },
                { 4, 6 },
        });
        assertThat(a.dist().data(), is(new double[][] {
                { 0, 5 },
                { 5, 0 },
        }));

        String csv = IOUtils.toString(getClass().getResourceAsStream("/pca/sandbox/client/resources/task2.csv"));
        Matrix m = Matrix.loadFromCsv(csv, ';', true, true);
        Matrix dist = m.dist();
        System.out.println(dist);
    }
}
