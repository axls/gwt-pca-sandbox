package pca.sandbox.math.transformations.pca;

import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import pca.sandbox.math.Matrix;


public class PcaTransformationTest {

	@Test
	public void test() {
		Matrix x = Matrix.of(new double[][] {
			{1, 4, 2},
			{3, 2, 5},
			{2, 3, 4},
			{4, 2, 3},
		});
		
		PcaTransformationResult result = new PcaTransformation().transform(x, 2);
		System.out.println("Scores");
		System.out.println("------------------------------------------------");
		System.out.println(result.getScores());
		System.out.println("Payloads");
		System.out.println("------------------------------------------------");
		System.out.println(result.getPayloads());
	}

	@Test
	public void peoples() throws IOException {
		String csv = IOUtils.toString(getClass().getResourceAsStream("/pca/sandbox/client/resources/peopleData.csv"));
		Matrix x = Matrix.loadFromCsv(csv , '.', true, true);
		System.out.println("Data");
		System.out.println("------------------------------------------------");
		System.out.println(x);
		System.out.println("Mean");
		System.out.println("------------------------------------------------");
		System.out.println(x.mean());
		System.out.println("Std");
		System.out.println("------------------------------------------------");
		System.out.println(x.std());
		
		x = x.autoscale();
		Matrix scaled = Matrix.copyOf(x);
		System.out.println("Autoscaled Data");
		System.out.println("------------------------------------------------");
		System.out.println(x);
		System.out.println("Mean");
		System.out.println("------------------------------------------------");
		System.out.println(x.mean());
		System.out.println("Std");
		System.out.println("------------------------------------------------");
		System.out.println(x.std());
		
		PcaTransformationResult result = new PcaTransformation().transform(x, 12);
		System.out.println("Scores");
		System.out.println("------------------------------------------------");
		Matrix scores = result.getScores();
		System.out.println(scores);
		System.out.println("Payloads");
		System.out.println("------------------------------------------------");
		Matrix payloads = result.getPayloads();
		System.out.println(payloads);
		System.out.println("Iterations");
		System.out.println("------------------------------------------------");
		System.out.println(Arrays.toString(result.getIterations()));
		
		System.out.println("Restored diff");
		System.out.println("------------------------------------------------");
		Matrix diff = scores.multiply(payloads).sub(scaled);
		System.out.println(diff);
		System.out.println("Mean");
		System.out.println("------------------------------------------------");
		System.out.println(diff.mean());
		System.out.println("Std");
		System.out.println("------------------------------------------------");
		System.out.println(diff.std());
	}
	
	@Test
	public void binary() throws IOException {
		String csv = IOUtils.toString(getClass().getResourceAsStream("/pca/sandbox/client/resources/task2.csv"));
		Matrix x = Matrix.loadFromCsv(csv , '.', true, true);
//		System.out.println("Data");
//		System.out.println("------------------------------------------------");
//		System.out.println(x);
		System.out.println("Mean");
		System.out.println("------------------------------------------------");
		System.out.println(x.mean());
		System.out.println("Std");
		System.out.println("------------------------------------------------");
		System.out.println(x.std());
		
		//x = x.autoscale();
//		Matrix scaled = Matrix.copyOf(x);
//		System.out.println("Autoscaled Data");
//		System.out.println("------------------------------------------------");
//		System.out.println(x);
//		System.out.println("Mean");
//		System.out.println("------------------------------------------------");
//		System.out.println(x.mean());
//		System.out.println("Std");
//		System.out.println("------------------------------------------------");
//		System.out.println(x.std());
		
		PcaTransformationResult result = new PcaTransformation().transform(x, 62);
		System.out.println("Scores");
		System.out.println("------------------------------------------------");
		Matrix scores = result.getScores();
		//scores.norm();
		System.out.println(scores);
//		System.out.println("Payloads");
//		System.out.println("------------------------------------------------");
		Matrix payloads = result.getPayloads();
//		System.out.println(payloads);
		System.out.println("Iterations");
		System.out.println("------------------------------------------------");
		System.out.println(Arrays.toString(result.getIterations()));
		
		System.out.println("Restored diff");
		System.out.println("------------------------------------------------");
		Matrix diff = scores.multiply(payloads).sub(x);
		System.out.println(diff);
		System.out.println("Mean");
		System.out.println("------------------------------------------------");
		System.out.println(diff.mean());
		System.out.println("Std");
		System.out.println("------------------------------------------------");
		System.out.println(diff.std());
		System.out.println("Diff L = " + diff.lambdas());
		System.out.println("Diff L0 = " + diff.lambdas().scalar());
	}

}
