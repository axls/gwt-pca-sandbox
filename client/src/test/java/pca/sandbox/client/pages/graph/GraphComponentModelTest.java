package pca.sandbox.client.pages.graph;

import java.io.IOException;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import pca.sandbox.client.components.GraphComponentModel;
import pca.sandbox.math.HCluster;
import pca.sandbox.math.Matrix;

public class GraphComponentModelTest {

    @Test
    public void test() throws IOException {
        String csv = IOUtils.toString(getClass().getResourceAsStream("/pca/sandbox/client/resources/task2.csv"));
        Matrix m = Matrix.loadFromCsv(csv, '.', true, true);

        HCluster cluster = HCluster.of(m);
        GraphComponentModel model = GraphComponentModel.create(cluster);
        System.out.println(model);
        
        Stream.of(model.links).filter(l -> "10".equals(l.source) || "10".equals(l.target)).forEach(System.out::println);
    }

}
